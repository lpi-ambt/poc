package com.ambitsoftware.lpi.poc;
import java.util.Iterator;

import lotus.domino.*;

public final class DocAttachmentParser implements Iterator {

private Session s;
private Document doc;
private Double count ;
private Iterator attIterator = null;
public  Double getCount() {
    return count;
}
public  DocAttachmentParser(Session s, Document doc) throws NotesException {
        this.s  = s;
        this.doc = doc;
        if (s!=null && doc !=null){
            this.count = (Double) s.evaluate("@Attachments", doc).elementAt(0);
            if (count.intValue() > 0){
                attIterator = s.evaluate("@AttachmentNames", doc).iterator();
                }
        }

}
    public boolean hasNext() {
        return count.intValue() > 0 ? attIterator.hasNext(): false;
    }

    public Object next() {
        return count.intValue() > 0 ? attIterator.next(): null;
    }
    private String nextAttName(){
        return count.intValue() > 0 ? attIterator.next().toString(): null;
    }

    public void remove() {
        if (count.intValue() > 0) attIterator.remove();
    }

    public String getAll(){

        StringBuilder sb = new StringBuilder();
        if (count.intValue()>0){

            while (hasNext()) {
                sb.append(nextAttName());
            }
        }

        return sb.toString();
    }

}