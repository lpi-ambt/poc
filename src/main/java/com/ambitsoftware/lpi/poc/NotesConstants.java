package com.ambitsoftware.lpi.poc;
public abstract class NotesConstants {
	public static final String CURRENT_MAILFILE_SPECIAL_URL = "notes:///0000000000000E00"; //$NON-NLS-1$
	public static final String CURRENT_ADDRESS_BOOK_SPECIAL_URL = "notes:///0000000000000E01"; //$NON-NLS-1$
}