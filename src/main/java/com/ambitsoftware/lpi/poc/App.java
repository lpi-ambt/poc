package com.ambitsoftware.lpi.poc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.EmbeddedObject;
import lotus.domino.Item;
import lotus.domino.NotesException;
import lotus.domino.NotesFactory;
import lotus.domino.NotesThread;
import lotus.domino.RichTextItem;
import lotus.domino.Session;

/**
 * Hello world!
 *
 */
@SuppressWarnings("restriction")
public class App extends NotesThread {
	public static void main(String argv[]) {
		App app = new App();
		app.start();
	}

	public void runNotes() {
		try {
			lotus.notes.NotesThread.sinitThread();
			Session s = NotesFactory.createSessionWithFullAccess("abcd@1234");
			Database db = NotesUtils.getDatabase(s, NotesConstants.CURRENT_MAILFILE_SPECIAL_URL);
			DocumentCollection dc = db.getAllDocuments();
			Document doc = dc.getFirstDocument();
			boolean saveFlag = false;
			while (doc != null) {
				System.out.println(doc.getItemValueString("Subject"));
				RichTextItem body = (RichTextItem) doc.getFirstItem("Body");
				if (body != null) {
					Vector v = body.getEmbeddedObjects();
					Enumeration e = v.elements();
					while (e.hasMoreElements()) {
						EmbeddedObject eo = (EmbeddedObject) e.nextElement();
						System.out.println(eo);
						if (eo.getType() == EmbeddedObject.EMBED_ATTACHMENT) {
							eo.extractFile("E:\\extracts\\" + eo.getSource());
							// eo.remove();
							saveFlag = true;
						}
					}
					if (saveFlag) {
						doc.save(true, true);
						saveFlag = false;
					}
				}
				doc = dc.getNextDocument();
			}

			// Map<File, byte[]> sources = new HashMap<File, byte[]>();
			// sources.putAll(getAttchments(doc));
			// while (doc != null) {
			// System.out.println(doc.getItemValueString("Subject"));
			// doc = vw.getNextDocument(doc);
			// sources.putAll(getAttchments(doc));
			// }
			// System.out.println(sources);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] readFully(InputStream input) throws IOException {
		byte[] buffer = new byte[8192];
		int bytesRead;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
		return output.toByteArray();
	}

	public static Map<File, byte[]> getAttchments(Document doc) throws IOException, NotesException {
		Map<File, byte[]> sources = new HashMap<File, byte[]>();
		RichTextItem body = (RichTextItem) doc.getFirstItem("Body");
		if (body != null) {
			List<EmbeddedObject> embeddedObjects = new ArrayList<EmbeddedObject>(body.getEmbeddedObjects());
			for (EmbeddedObject embeddedObject : embeddedObjects) {
				if (embeddedObject.getType() != EmbeddedObject.EMBED_ATTACHMENT) {
					continue;
				}
				try {
					sources.put(new File(embeddedObject.getName()), readFully(embeddedObject.getInputStream()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sources;
	}
}