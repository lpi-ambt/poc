package com.ambitsoftware.lpi.poc;

import java.net.ConnectException;

import lotus.domino.Database;
import lotus.domino.NotesError;
import lotus.domino.NotesException;
import lotus.domino.Session;

@SuppressWarnings("restriction")
public abstract class NotesUtils {

	// FIXME [rev] change an url type to URL
	public static Database getDatabase(Session session, String url) throws NotesException, ConnectException {
		if (url.compareTo(NotesConstants.CURRENT_MAILFILE_SPECIAL_URL) == 0) {
			return openMailDatabase(session);
		}
		if (url.compareTo(NotesConstants.CURRENT_ADDRESS_BOOK_SPECIAL_URL) == 0) {
			return openAddressBookDatabase(session);
		}
		return (Database) session.resolve(url);
	}

	private static Database openMailDatabase(Session session) throws NotesException, ConnectException {
		try {
			return session.getDbDirectory(null).openMailDatabase();
		} catch (NotesException e) {
			throw new ConnectException(
					"Could not open the current user's Mail database. Reason (" + e.id + "): " + e.text);
		}
	}

	private static Database openAddressBookDatabase(Session session) throws NotesException, ConnectException {
		try {
			return (Database) session.resolve(NotesConstants.CURRENT_ADDRESS_BOOK_SPECIAL_URL);
		} catch (NotesException e) {
			switch (e.id) {
			case NotesError.NOTES_ERR_URL_NOT_RESOLVED:
				// FIXME add a get addressbook from ini
			case NotesError.NOTES_ERR_DBOPEN_FAILED:
				// FIXME add exception
				// throw new ConnectException( "Could not open the current
				// user's Mail database" );
			}
			throw e;
		}
	}
}